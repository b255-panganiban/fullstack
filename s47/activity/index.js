const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

function enterFullName() {
	const fullName = `${txtFirstName.value} ${txtLastName.value}`;
	spanFullName.innerHTML = fullName;
	console.log(event.target);
	console.log(event.target.value);
}

txtFirstName.addEventListener('keyup', enterFullName);
txtLastName.addEventListener('keyup', enterFullName);